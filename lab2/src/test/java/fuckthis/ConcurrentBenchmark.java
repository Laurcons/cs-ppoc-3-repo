package fuckthis;

import fuckthis2.ConcurrentHashMapRepository;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

@Threads(4)
@Fork(1)
public class ConcurrentBenchmark {
    @Benchmark
    @Warmup(iterations = 2, time = 5)
    @Measurement(iterations = 2, time = 5)
    @BenchmarkMode(Mode.Throughput)
    public void doStuff(ThreadState state, Blackhole consumer) {
        for (int i = 0; i < state.size; i++) {
            state.repo.add(i, i+ 1);
        }
        for (int i = 0; i < state.size; i++) {
            consumer.consume(state.repo.contains(i));
        }
        for (int i = 0; i < state.size; i++) {
            state.repo.remove(i);
        }
        for (int i = 0; i < state.size; i++) {
            consumer.consume(state.repo.contains(i));
        }
    }
}
