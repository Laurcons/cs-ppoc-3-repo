package fuckthis;

import fuckthis2.ConcurrentHashMapRepository;
import org.openjdk.jmh.annotations.*;

@State(Scope.Benchmark)
public class ThreadState {
    public ConcurrentHashMapRepository<Integer, Integer> repo;
    @Param({"20", "50"})
    public int size;

    @Setup(Level.Iteration)
    public void setup() {
        repo = new ConcurrentHashMapRepository<>();
    }
}
