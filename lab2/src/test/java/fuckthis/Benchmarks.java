package fuckthis;

import fuckthis2.ArrayListRepository;
import fuckthis2.HashSetRepository;
import fuckthis2.IRepository;
import fuckthis2.TreeSetRepository;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

@Fork(1)
@BenchmarkMode(Mode.Throughput)
@Warmup(iterations = 2, time = 5)
@Measurement(iterations = 2, time = 5)
@State(Scope.Thread)
public class Benchmarks {
//    @Param({"1", "31", "65", "101", "103", "1024", "10240", "65535", "21474836"})
    @Param({"20", "50"})
    public int size;

    @Param({"hashSet", "arrayList", "treeSet"})
    public String repoType;

    IRepository<Integer> repo;

    @Setup
    public void setup() {
        if (Objects.equals(repoType, "hashSet")) {
            repo = new HashSetRepository<>();
        } else if (Objects.equals(repoType, "arrayList")){
            repo = new ArrayListRepository<>();
        } else if (Objects.equals(repoType, "treeSet")) {
            repo = new TreeSetRepository<>();
        }
    }

    @Benchmark
    public void add(Blackhole consumer) {
        for (int i = 0; i < size; i++) {
            repo.add(i);
        }
        for (int i = 0; i < size; i++) {
            consumer.consume(repo.contains(i));
        }
        for (int i = 0; i < size; i++) {
            repo.remove(i);
        }
        for (int i = 0; i < size; i++) {
            consumer.consume(repo.contains(i));
        }
    }
}
