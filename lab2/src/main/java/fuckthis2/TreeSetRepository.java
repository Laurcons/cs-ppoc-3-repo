package fuckthis2;

import java.util.HashSet;
import java.util.TreeSet;

public class TreeSetRepository<T extends Comparable<? super T>> implements IRepository<T> {
    private TreeSet<T> set;

    public TreeSetRepository() {
        set = new TreeSet<>();
    }
    @Override
    public void add(T thing) {
        set.add(thing);
    }

    @Override
    public void remove(T thing) {
        set.remove(thing);
    }

    @Override
    public Boolean contains(T thing) {
        return set.contains(thing);
    }
}
