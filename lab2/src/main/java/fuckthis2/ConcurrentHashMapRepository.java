package fuckthis2;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapRepository<K, V> {
    private ConcurrentHashMap<K, V> map;

    public ConcurrentHashMapRepository() {
        map = new ConcurrentHashMap<>();
    }

    public void add(K key, V val) {
        map.put(key, val);
    }

    public void remove(K key) {
        map.remove(key);
    }

    public Boolean contains(K key) {
        return map.contains(key);
    }
}
