package fuckthis2;

import java.util.HashSet;

public class HashSetRepository<T> implements IRepository<T> {
    private HashSet<T> set;

    public HashSetRepository() {
        set = new HashSet<>();
    }
    @Override
    public void add(T thing) {
        set.add(thing);
    }

    @Override
    public void remove(T thing) {
        set.remove(thing);
    }

    @Override
    public Boolean contains(T thing) {
        return set.contains(thing);
    }
}
