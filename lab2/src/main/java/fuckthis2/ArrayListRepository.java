package fuckthis2;

import java.util.ArrayList;

public class ArrayListRepository<T> implements IRepository<T> {
    private ArrayList<T> lst;

    public ArrayListRepository() {
        lst = new ArrayList<>();
    }

    @Override
    public void add(T thing) {
        lst.add(thing);
    }

    @Override
    public void remove(T thing) {
        lst.remove(thing);
    }

    @Override
    public Boolean contains(T thing) {
        return lst.contains(thing);
    }
}
