import com.github.kwhat.jnativehook.GlobalScreen;
import com.github.kwhat.jnativehook.NativeHookException;
import com.github.kwhat.jnativehook.keyboard.NativeKeyEvent;
import com.github.kwhat.jnativehook.keyboard.NativeKeyListener;
import message.Event;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.BlockingQueue;

public class ViewManager implements NativeKeyListener {
    BlockingQueue<Event> in;
    BlockingQueue<Event> out;
    String inBuf = "";
    Boolean redraw = true;

    public ViewManager(BlockingQueue<Event> in, BlockingQueue<Event> out) {
        this.in = in;
        this.out = out;
    }

    @Override
    public void nativeKeyPressed(NativeKeyEvent nativeEvent) {
//        System.out.println(nativeEvent.getKeyCode());
        inBuf += nativeEvent.getKeyChar();
        redraw = true;
    }

    public void takeOver() throws IOException, NativeHookException {
        // clear console
        System.out.print("\033[H\033[2J");
        System.out.flush();

        while (true) {
            if (redraw) {
                System.out.print("\r\r> " + inBuf);
                redraw = false;
            }
        }
    }
}
