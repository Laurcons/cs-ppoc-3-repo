import message.Event;
import message.MessageEvent;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;

public class ConnectionReceiverThread extends Thread {
    private final Connection conn;

    ConnectionReceiverThread(Connection conn) {
        this.conn = conn;
    }

    public void run() {
        while (true) {
            try {
                Event ev = this.conn.receive();
                if (ev instanceof MessageEvent) {
                    MessageEvent msg = (MessageEvent) ev;
                    System.out.println("THEM: " + msg.getMessage());
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
