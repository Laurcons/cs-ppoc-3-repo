import message.Event;
import message.MessageEvent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.BlockingQueue;

public class ConnectionSenderThread extends Thread {
    private final Connection conn;

    ConnectionSenderThread(Connection conn) {
        this.conn = conn;
    }

    public void run() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            try {
                String msg = reader.readLine();
                this.conn.send(new MessageEvent(msg));
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }
}
