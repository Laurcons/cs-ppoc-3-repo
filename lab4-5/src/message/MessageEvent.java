package message;

public class MessageEvent implements Event {
    String message;

    public MessageEvent(String m) {
        message = m;
    }

    public String getMessage() {
        return message;
    }
}
