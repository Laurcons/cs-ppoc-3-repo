import com.google.gson.Gson;
import message.Event;
import message.MessageEvent;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Connection {
    private Socket sock;
    private PrintWriter out;
    private BufferedReader in;

    private final List<Class<? extends Event>> messageTypes = List.of(
            MessageEvent.class
    );

    public static Connection createAsServer(int port) throws IOException {
        ServerSocket serv = new ServerSocket(port);
        Socket sock = serv.accept();
        return new Connection(sock);
    }

    public static Connection createAsClient(String ip, int port) throws IOException {
        Socket sock = new Socket(ip, port);
        return new Connection(sock);
    }

    private Connection(Socket sock) throws IOException {
        this.sock = sock;
        this.out = new PrintWriter(sock.getOutputStream(), true);
        this.in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
    }

    public Event receive() throws IOException {
        String msg = this.in.readLine();
        String[] parts = msg.split(";");
        String json = parts[1];
        Gson gson = new Gson();
        for (Class<? extends Event> cls : this.messageTypes) {
            if (cls.getName().equals(parts[0])) {
                return gson.fromJson(json, cls);
            }
        }
        throw new IOException("Invalid event received");
    }

    public void send(Event e) throws IOException {
        Gson gson = new Gson();
        String json = gson.toJson(e);
        String name = e.getClass().getName();
        String msg = name + ";" + json;
        this.out.println(msg);
    }

    public void stop() throws IOException {
        in.close();
        out.close();
        sock.close();
    }
}
