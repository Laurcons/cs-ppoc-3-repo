import com.github.kwhat.jnativehook.NativeHookException;
import message.Event;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Main {
    public static void main(String[] args) throws IOException, NativeHookException {
        // Enter data using BufferReader
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Ultimate Chatter 3000\n");
        System.out.println("If you want to be a server, type PORT");
        System.out.println("If you want to be a client, type IP:PORT");
        String instruction = reader.readLine();

        String[] parts = instruction.split(":");
        Connection conn;
        if (parts.length == 2) {
            // start as client
            conn = Connection.createAsClient(parts[0], Integer.parseInt(parts[1]));
        } else {
            // start as server
            conn = Connection.createAsServer(Integer.parseInt(parts[0]));
        }
        ConnectionSenderThread sender = new ConnectionSenderThread(conn);
        ConnectionReceiverThread receiver = new ConnectionReceiverThread(conn);
        sender.start();
        receiver.start();
    }
}