package com.example;

public interface IOperation {
    Double doOperation(Double first, Double second);
}
