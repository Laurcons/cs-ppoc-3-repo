package com.example;

public class Addition implements IOperation {

    @Override
    public Double doOperation(Double first, Double second) {
        return first + second;
    }
}
