package com.example;

public class Multiplication implements IOperation {
    @Override
    public Double doOperation(Double first, Double second) {
        return first * second;
    }
}
