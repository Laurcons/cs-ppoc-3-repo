package com.example;

public class Calculator {
  public Double doOperation(Double first, Double second, IOperation op) {
    return op.doOperation(first, second);
  }

}
