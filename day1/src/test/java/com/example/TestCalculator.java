package com.example;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestCalculator {

    public Calculator calculator;

    @Before
    public void setup() {
        calculator = new Calculator();
    }

    @Test
    public void additionShouldReturnCorrectly1() {
        Double result =
                calculator.doOperation(3.4, 5.6, new Addition());
        assertEquals(9.0, result, 0.01);
    }

    @Test
    public void additionShouldReturnCorrectly2() {
        Double result =calculator.doOperation(
                100000.,
                200000.,
                new Addition());
        assertEquals(300000., result, 0.01);
    }

    @Test
    public void multiplicationShouldReturnCorrectly() {
        Double result = calculator.doOperation(
                500.,
                500.,
                new Multiplication());
        assertEquals(250000, result, 0.001);
    }

}
